﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laskin1
{
   
   public abstract class Laskin
    {
        public int Luku1 { get; set; }
        public int Luku2 { get; set; }

        public abstract int Laske();

    }
    
}
