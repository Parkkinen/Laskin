﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laskin1
{
    class Program
    {
  
    
        static void Main(string[] args)
        {
            Dictionary<string, Laskin> operaatiot = new Dictionary<string, Laskin>();
            operaatiot.Add("+", new Yhteenlaskin());
            operaatiot.Add("-", new Vahennyslaskin());
            operaatiot.Add("*", new Kertolaskin());
            operaatiot.Add("/", new Jakolaskin());

            int i = 1;
            Console.WriteLine("Moi, olen yksinkertainen laskin\nOsaan laskea kaksi lukua keskenään\nOperaattoreilla +, -, *, /\n");

            while (i != 0)
            {
                Console.WriteLine("Anna luku1: ");
                int temp = int.Parse(Console.ReadLine());


                Console.WriteLine("Anna operaatio: ");
                String operaatio = Console.ReadLine();

                // Console.WriteLine("Anna luku1: ");
                operaatiot[operaatio].Luku1 = temp; //int.Parse(Console.ReadLine());

                Console.WriteLine("Anna luku2: ");
                operaatiot[operaatio].Luku2 = int.Parse(Console.ReadLine());

                int Tulos = operaatiot[operaatio].Laske();
                Console.WriteLine("\nTulos: " + Tulos+"\n");


     
            }
        }
    }
}
